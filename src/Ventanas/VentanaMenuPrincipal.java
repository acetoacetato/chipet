package Ventanas;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class VentanaMenuPrincipal {
	public void start(Stage stage) throws Exception {

        //Cargar el contenido del archivo /Ventana.fxml, procesarlo y crear el
        //contenido a partir del mismo.
        Parent root = FXMLLoader.load(getClass().getResource("Ventana.fxml"));

        //Asignar las UI creadas desde *.fxml a un Scene
        Scene scene = new Scene(root);

        //Asignar el Scene a la ventana principal o Stage y mostrarla.
        stage.setScene(scene);
        stage.show();
    }
}
