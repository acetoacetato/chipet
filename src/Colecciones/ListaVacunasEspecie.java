package Colecciones;

import java.sql.SQLException;
import java.util.ArrayList;

import Objetos.Vacuna;
import SQL.Consultas;

public class ListaVacunasEspecie extends ListaVacunas {
	
	
	
	public ListaVacunasEspecie(String tipo) throws SQLException {
		super(tipo);
		
	}
	
	public ListaVacunasEspecie(String tipo, ArrayList<Vacuna> vacunas) throws SQLException {
		super(tipo, vacunas);
	}

	@Override
	protected void agregaraBD(Vacuna v) throws SQLException {
		Consultas consulta = Consultas.getConsultas();
		//consulta.agregarVacunasEspecie(this.getId(), v);
	}
	
	@Override
	public void eliminarDeBd(String nroSerie) throws SQLException {
		getConsulta().eliminarVacunaEspecie(this.getId(), nroSerie);
	}

	public ArrayList<Vacuna> obtenerLista(){
		return super.obtenerLista();
	}

	

}
