package Colecciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import Objetos.Especie;
import Objetos.Vacuna;
import SQL.Consultas;

public class MapaEspecies {
	private Hashtable<String, Especie> mapaEspecies;
	
	
	public MapaEspecies() throws SQLException {
		mapaEspecies = new Hashtable<String, Especie>();
		
		importar();
		
	}
	
	private void importar() throws SQLException {
		Consultas consultas = Consultas.getConsultas();
		ResultSet rs = consultas.obtenerEspecies();
		while(rs.next()) {
			Especie e = new Especie(rs);
			mapaEspecies.putIfAbsent(e.getTipo(), e);
		}
	}
	
	
	public void agregarEspecie(String tipo, String descripcion,  ArrayList<Vacuna> vacunas) throws SQLException {
		Consultas consultas = Consultas.getConsultas();
		Especie e = new Especie(tipo, descripcion, vacunas);
		consultas.agregarEspecie(e);
	}
	
	public void eliminarEspecie(String tipo) throws SQLException {
		Consultas consultas = Consultas.getConsultas();
		if( mapaEspecies.remove(tipo) != null ) 
			consultas.eliminarEspecie(tipo);
		
	}
	
	public Especie modificarEspecie(String tipo, String descripcion) throws SQLException {
		Consultas consultas = Consultas.getConsultas();
		Especie e = mapaEspecies.get(tipo);
		if(e != null) {
			e.modificar(descripcion);
			consultas.modificarEspecie(tipo, descripcion);
		}
		return e;
	}
	


	public Especie obtenerEspecie(String especie) {
		return mapaEspecies.get(especie);
	}
	
	
}
