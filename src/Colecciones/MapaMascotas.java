package Colecciones;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Objetos.Especie;
import Objetos.Mascota;
import Objetos.Vacuna;

public class MapaMascotas {
	private Map<String,Mascota> map = new HashMap<String,Mascota>();
	
	
	
	public Mascota agregarMascota(String nombre, String  idChip,String sexo, Especie especie, ArrayList<Vacuna> vacunas, int edad, String duenio) throws SQLException {
		//Consultas consultaMascota = new Consultas();
		//String idMascota=""+consultaMascota.getIdChip(nombre);
			Mascota mascota = new Mascota(nombre, idChip, sexo, especie,vacunas, edad, false, duenio );
			if(map.put(idChip,mascota) != null) {
				return map.get(idChip);
			}
			return null;
	}


/**
 * Obtener mascota por el idChip
 * @param idChip
 * @return
 */
	public Mascota obtenerMascota (String idChip) {
		return (Mascota)map.get(idChip);
	
}

/**
 * Eliminar mascota por el idChip
 * @param idChip
 * @return
 */

	public boolean eliminarMascota(String idChip) {
	  map.remove(idChip);
	  return false;
  }
	
}