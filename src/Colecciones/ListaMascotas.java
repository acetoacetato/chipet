package Colecciones;

import java.awt.List;
import java.sql.SQLException;
import java.util.ArrayList;

import Objetos.Especie;
import Objetos.Mascota;
import Objetos.Vacuna;

public class ListaMascotas {

	private ArrayList<Mascota> listaDeMascotas = new ArrayList <Mascota>();

	
	
	/**
	 * Agrega una mascota a la lista
	 * @param nombre
	 * @param idChip
	 * @param sexo
	 * @param especie
	 * @param color
	 * @param edad
	 * @throws SQLException
	 */
	public void agregar_mascota(String nombre, String idChip, String sexo, Especie especie, 
							ArrayList<Vacuna> vacunas, int edad, String duenio) throws SQLException {
		
		//Agregar una mascota a la lista//
		Mascota mascota = new Mascota(nombre, idChip, sexo, especie,vacunas, edad, false, duenio );
		listaDeMascotas.add(mascota);
		//mascotaSql.agregaMascota(nombre,idChip,sexo,especie,color,edad);
		
		
	}
	
	/**
	 * Obtiene lista de mascotas a traves del sexo
	 * @return
	 */
	
	public List getMascotas() {
		List lista = new List();
		for(int i=0; i<listaDeMascotas.size();i++) {
			
			if((listaDeMascotas.get(i).getSexo()).equals("  "))
				break;
				lista.add(listaDeMascotas.get(i).getSexo());
		}
		return lista;
	}
	
	/**
	 * Obtiene a la mascota a traves del nombre
	 * @param nombre
	 * @return
	 */
	
	public Mascota getMascota(String nombre) {
		Mascota mascota;
		for(int i=0; i<listaDeMascotas.size(); i++) {
			mascota = listaDeMascotas.get(i);
			
			if(mascota.getNombre().equals(nombre))
				return mascota;
		}
		return null;
	}
	
	/**
	 * Eliminar una mascota por su nombre
	 */
	
	public boolean eliminarMascota(String nombre) {
		Mascota mascota;
		/*Consultas c = new Consultas();
		c.eliminarMascota(nombre);*/
		
		for(int i=0; i<listaDeMascotas.size(); i++) {
			mascota = listaDeMascotas.get(i);
			
			if(mascota.getNombre().equals(nombre)) {
				listaDeMascotas.remove(i);
				return true;
			}
		}
		return false;
	}

	public void agregarMascota(Mascota m) {
		if(!listaDeMascotas.contains(m))
			listaDeMascotas.add(m);
		
	}
	
	
}
