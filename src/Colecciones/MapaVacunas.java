package Colecciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import Objetos.Vacuna;
import SQL.Consultas;

public class MapaVacunas {
	private Hashtable<String, Vacuna> vacunas;
	private Consultas consulta;
	
	public MapaVacunas() throws SQLException {
		vacunas = new Hashtable<String, Vacuna>();
		consulta = Consultas.getConsultas();
		importar();
	}
	
	private void importar() throws SQLException {
		ResultSet rs = consulta.obtenerVacunas();
		while(rs.next()) {
			vacunas.putIfAbsent(rs.getString("nroSerie"), new Vacuna(rs));
		}
	}
	
	
	
	public void agregarVacuna(Vacuna v) throws SQLException {
		if(vacunas.putIfAbsent(v.getNroSerie(), v) == null) {
			consulta.agregarVacuna(v);
		}
	}
	
	public void eliminarVacuna(Vacuna v) throws SQLException {
	
		if(vacunas.remove(v.getNroSerie()) != null) {
			consulta.eliminarVacuna(v);
		}
	}
	
	
	
	public void editarVacuna(String nroSerie, String nombre) throws SQLException {
		Vacuna v = vacunas.get(nroSerie);
		if(v != null) {
			v.setNombre(nombre);
			consulta.editarVacuna(nroSerie, nombre);
		}
	}
	
	public ArrayList<Vacuna> getVacunas(ArrayList<String> vacunas){
		ArrayList<Vacuna> vacunitas = new ArrayList<Vacuna>();
		for(String nroSerie : vacunas) {
			Vacuna v = this.vacunas.get(nroSerie);
			if(v != null && !vacunitas.contains(v))
				vacunitas.add(v);
		}
		
		return vacunitas;
		
	}
	
	
	
}
