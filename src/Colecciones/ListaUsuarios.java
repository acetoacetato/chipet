package Colecciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import Excepciones.MascotitaException;
import Excepciones.RegistroFallidoException;
import Objetos.Administrador;
import Objetos.Duenio;
import Objetos.Mascota;
import Objetos.Usuario;
import SQL.Consultas;

public class ListaUsuarios {

	private Hashtable<String, Usuario> mapaUsuarios;
	

	
	public ListaUsuarios() throws SQLException {
		mapaUsuarios = new Hashtable<String, Usuario>();
		importar();
		
		
	}
	
	private void importar() throws SQLException {
		Consultas consultas = Consultas.getConsultas();
		ResultSet rs = consultas.obtenerUsuarios();
		while(rs.next()) {
			Usuario usr =  (rs.getBoolean("adm"))? new Administrador(rs) : new Duenio(rs);
			mapaUsuarios.put(usr.getRut(), usr);
		}
	}
	
	
	/**
	 * Obtiene al usuario a traves del rut
	 * @param rut
	 * @return
	 */
	
	public Usuario getUsuario(String rut) {
		Usuario usuario;
		usuario = mapaUsuarios.get(rut);
			
		if(usuario != null && usuario.getRut().equals(rut))
			return usuario;
		return null;
	}
	
	/**
	 * Elimina a un usuario por su rut
	 */
	
	public boolean eliminarUsuario(String rut) {
		Usuario usuario;
		/*Consultas c = new Consultas();
		c.eliminarMascota(nombre);*/
		
		for(int i=0; i<mapaUsuarios.size(); i++) {
			usuario = mapaUsuarios.get(i);
			
			if(usuario.getRut().equals(rut)) {
				mapaUsuarios.remove(i);
				return true;
			}
		}
		return false;
	}

	public void agregarUsuario(String nombre, String rut, int edad, String direccion,String contrasena) throws SQLException, RegistroFallidoException {
		Consultas consultas = Consultas.getConsultas();
		Duenio duenio = new Duenio(nombre,rut,edad,direccion,contrasena);
		if(mapaUsuarios.putIfAbsent(rut, duenio) != null)
			throw new RegistroFallidoException("El usuario con rut " + rut + " ya se encuentra registrado.");
		consultas.registrarUsuario(nombre, rut, edad, direccion, contrasena);
		
	}

	public void agregarMascota(Mascota m) throws MascotitaException{
		String rut = m.getDuenio();
		
		Usuario usr = mapaUsuarios.get(rut);
		if(usr == null)
			throw new MascotitaException("El usuario no existe en el sistema.");
		usr.agregarMascota(m);
		
		
	}

	

	
	
}


