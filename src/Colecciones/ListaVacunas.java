package Colecciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;


import Objetos.Vacuna;
import SQL.Consultas;

public class ListaVacunas {
	private Hashtable<String, Vacuna> vacunas;
	private Consultas consulta;
	private String id;
	
	public ListaVacunas(String chip, ArrayList<Vacuna> vacunitas) throws SQLException {
		vacunas = new Hashtable<String, Vacuna>();
		consulta = Consultas.getConsultas();
		this.id = chip;
		
		for(Vacuna v : vacunitas) {
			if( vacunas.putIfAbsent(v.getNroSerie(), v) == null) {
				consulta.agregarVacunasMascota(chip, v);
			}
			
		}
		
	}
	
	
	
	public ListaVacunas(String chip) throws SQLException {
		vacunas = new Hashtable<String, Vacuna>();
		consulta = Consultas.getConsultas();
		this.id = chip;
		importar();
		
		
	}
	
	
	
	public void importar() throws SQLException {
		ResultSet rs = consulta.obtenerVacunasMascota(id);
		while(rs.next()) {
			Vacuna vacuna = new Vacuna(rs);
			vacunas.putIfAbsent(vacuna.getNroSerie(),vacuna);
		}

	}
	
	protected void agregaraBD(Vacuna v) throws SQLException {
		consulta.agregarVacunasMascota(id, v);
	}
	
	public void agregarVacuna(ArrayList<Vacuna> vacunitas) throws SQLException {
		for(Vacuna v : vacunitas) {
			vacunas.putIfAbsent(v.getNroSerie(), v);
			if(!vacunas.contains(v))
				agregaraBD(v);
		}
	}
	
	public void agregarVacuna(Vacuna... vacunitas) throws SQLException {
		for(Vacuna v : vacunitas) {
			vacunas.putIfAbsent(v.getNroSerie(), v);
			if(!vacunas.contains(v))
				agregaraBD(v);
		}
		
	}
	public Vacuna obtenerVacuna(String nroSerie) {
		return vacunas.get(nroSerie);
	}
	
	public Iterator<Vacuna> obtenerVacunas(){
		return vacunas.values().iterator();
	}

	public void eliminarDeBd(String nroSerie) throws SQLException {
		consulta.eliminarVacunaMascota(id, nroSerie);
	}
	

	
	public void eliminarVacuna(String nroSerie) throws SQLException {	
		if( vacunas.remove(nroSerie) != null) {
			eliminarDeBd(nroSerie);
		}
	}
	
	protected void editarDeBd(String nroSerie, String nombre) throws SQLException {
		consulta.editarVacuna(nroSerie, nombre);
	}
	
	public void EditarVacuna(String nroSerie, String nombre) throws SQLException {
		Vacuna v = vacunas.get(nroSerie);
		if(v != null) {
			v.setNombre(nombre);
			editarDeBd(nroSerie, nombre);
		}
	}
	
	public void modificar(Vacuna[] vacunas2) {
		
		
	}
	
	
	protected Consultas getConsulta() {
		return consulta;
	}
	public String getId() {
		return id;
	}
	
	public ArrayList<Vacuna> obtenerLista(){
		return new ArrayList<Vacuna> (vacunas.values());
	}
	
	
	
	
}
