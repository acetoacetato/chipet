package application;

import java.io.IOException;
import java.sql.SQLException;

import Excepciones.MascotitaException;
import Objetos.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class MenuAdministrador {
		@FXML
		private Button AdministrarUsuarios;
		
		@FXML
		private Button AdministrarEspecies;
		
		@FXML
		private Button reporte;
		
		@FXML
		private Button defuncion;
		
		@FXML
		private TextField claveChip;

		public void administrarUsuarios(ActionEvent evento) {
			Stage primaryStage= new Stage();
			Parent root = null;
			try {
				root = FXMLLoader.load(getClass().getResource("/application/VentanaAdministradorUsuario.fxml"));
			} catch (IOException noHayEntrada) {
				// TODO Auto-generated catch block
				noHayEntrada.printStackTrace();
			}
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			closeAction(evento);
		}
		
		public void administrarEspecies(ActionEvent evento) {
			Stage primaryStage= new Stage();
			Parent root = null;
			try {
				root = FXMLLoader.load(getClass().getResource("/application/VentanaAdministradorEspecie.fxml"));
			} catch (IOException noHayEntrada) {
				// TODO Auto-generated catch block
				noHayEntrada.printStackTrace();
			}
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			closeAction(evento);
		}
		
		public void defuncion(ActionEvent evento)throws SQLException {
			
				SistemaChip mascota = ClaseSistema.getSistemaChip();
				boolean aux = mascota.eliminarMascota(claveChip.getText());
				if(aux!= false) {
					System.out.println("MascotaEliminada");
				}
				
		}
		
		public void reporte() {
			
		
			
			
			
			
			
			
		}
		
		
		@FXML
		private void closeAction(ActionEvent event) {
		
			 Stage stage;
		     stage=(Stage) AdministrarUsuarios.getScene().getWindow();
		     stage.close();
		}
}
