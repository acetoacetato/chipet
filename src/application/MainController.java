package application;

import java.io.IOException;
import java.sql.SQLException;

import Objetos.Usuario;
import Objetos.Validador;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class MainController {
	

	@FXML
	private Label lblStatus;
	
	@FXML
	private TextField txtIdUsuario;
	
	@FXML
	private TextField txtContrasenia;
	
	
	
	/**
	 * Si los parametros del id del usuario y su contrase�a son los correctos entra al men�
	 * @param evento
	 * @throws IOException
	 * @throws SQLException 
	 */
	
	public void login(ActionEvent evento) throws IOException, SQLException {
		
		SistemaChip usuario = ClaseSistema.getSistemaChip();
		Usuario aux=usuario.obtenerUsuario(txtIdUsuario.getText(), txtContrasenia.getText());
		if(!Validador.validarRut(txtIdUsuario.getText())) {
			lblStatus.setText("Formato del rut incorrecto, debe contener 8 números seguidos de un guión y otro número o un k mayúscula o minúscula");
			return;
		}
		if(aux != null) {
			
			lblStatus.setText("Entrando...");
			closeAction(evento);
			Stage primaryStage= new Stage();
			Parent root = null;
			try {
				root = FXMLLoader.load(getClass().getResource("/application/Main.fxml"));
			} catch (IOException noHayEntrada) {
				noHayEntrada.printStackTrace();
			}
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
		}else {
			lblStatus.setText("Usuario/Contrasenia incorrecta");
		}
	}

	public void registro(ActionEvent evento) throws IOException {
		
			
	
			Stage primaryStage= new Stage();
			Parent root = null;
			try {
				root = FXMLLoader.load(getClass().getResource("/application/VentanaRegistro.fxml"));
			} catch (IOException noHayEntrada) {
				// TODO Auto-generated catch block
				noHayEntrada.printStackTrace();
			}
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			closeAction(evento);
		
			
	}
	
	
	@FXML
	private void closeAction(ActionEvent event) {
	
		 Stage stage;
	     stage=(Stage) lblStatus.getScene().getWindow();
	     stage.close();
	}
	
}
