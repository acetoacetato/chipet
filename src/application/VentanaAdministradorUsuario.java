package application;

import java.io.IOException;
import java.sql.SQLException;

import Objetos.Duenio;
import Objetos.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class VentanaAdministradorUsuario {
	
	private TextField buscarRut;
	
	private TextField nombreUsuario;
	
	private TextField direccionUsuario;
	
	private TextField buscarYEliminar;
	
		
	
	public void BuscarUsuario(ActionEvent evento)throws SQLException {
		String user =buscarRut.getText();
		SistemaChip  duenio= ClaseSistema.getSistemaChip();
		Duenio aux = duenio.obtenerDuenio(user);
		if(duenio != null) {
			nombreUsuario.setText(aux.getNombre());
			direccionUsuario.setText(aux.getDireccion());
			
		}
		
				
	}
	
	public void Aplicar(ActionEvent evento)throws SQLException {
		String user =buscarRut.getText();
		SistemaChip  duenio= ClaseSistema.getSistemaChip();
		Duenio aux = duenio.obtenerDuenio(user);
		if(aux != null) {
			aux.setNombre(nombreUsuario.getText());
			aux.setDireccion(direccionUsuario.getText());
			System.out.println("Usuario Modificado");
		}	
	}
	
	public void Mascotas(ActionEvent evento) {
		Stage primaryStage= new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/application/VentanaEdicionMascotas.fxml"));
		} catch (IOException noHayEntrada) {
			// TODO Auto-generated catch block
			noHayEntrada.printStackTrace();
		}
		Scene scene = new Scene(root,400,400);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		closeAction(evento);
		
		
	}
	
	public void eliminarUsuario(ActionEvent evento)throws SQLException {
		String user =buscarRut.getText();
		SistemaChip  duenio= ClaseSistema.getSistemaChip();
		if(duenio != null) {
		duenio.eliminarUsuario(user);
		}
	}
	
	@FXML
	private void closeAction(ActionEvent event) {
	
		 Stage stage;
	     stage=(Stage) buscarRut.getScene().getWindow();
	     stage.close();
	}
	
	
		
}
	
	

