package application;

import java.io.IOException;
import java.sql.SQLException;

import Excepciones.RegistroFallidoException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class VentanaRegistroUsuario {
	@FXML
	private TextField nombre;
	
	@FXML
	private TextField rut;

	@FXML
	private TextField edad;
	 @FXML
	 private TextField direccion;
	@FXML
	private TextField contrasena;
	@FXML
	private Button volverAtras;
	
	@FXML
	private Label lblAviso;

		public void registrar(ActionEvent event) throws SQLException {
			lblAviso.setVisible(false);
			SistemaChip sistema = ClaseSistema.getSistemaChip();
			if(nombre.getText()!=null && rut.getText()!=null && edad.getText()!=null && contrasena.getText()!=null && direccion.getText()!=null) {
				try {
					sistema.registraUsuario(nombre.getText(),rut.getText(),edad.getText(),direccion.getText(),contrasena.getText());
				} catch (NumberFormatException e) {
					lblAviso.setText("La edad sólo debe contener números");
					lblAviso.setVisible(true);
				} catch(RegistroFallidoException e2) {
					lblAviso.setText(e2.getMessage());
					lblAviso.setVisible(true);
				}
			}
		}
		

		

		public void volver(ActionEvent event) {
			Stage primaryStage= new Stage();
			Parent root = null;
			try {
				root = FXMLLoader.load(getClass().getResource("/application/Login.fxml"));
			} catch (IOException noHayEntrada) {
				// TODO Auto-generated catch block
				noHayEntrada.printStackTrace();
			}
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			Stage stage;
		    stage=(Stage) volverAtras.getScene().getWindow();
		    stage.close();
			
			
		}
}
