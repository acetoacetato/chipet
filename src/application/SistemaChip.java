package application;

import java.sql.SQLException;
import java.util.ArrayList;

import Colecciones.ListaUsuarios;
import Colecciones.MapaEspecies;
import Colecciones.MapaMascotas;
import Colecciones.MapaVacunas;
import Excepciones.MascotitaException;
import Excepciones.RegistroFallidoException;
import Objetos.Duenio;
import Objetos.Especie;
import Objetos.Mascota;
import Objetos.Usuario;
import Objetos.Vacuna;

public class SistemaChip {

	private MapaMascotas mapaMascotas;
	private ListaUsuarios listaUsuarios;
	private MapaEspecies mapaEspecies;
	private MapaVacunas mapaVacunas;
	
	
	
	
	
	
	public SistemaChip() throws SQLException {
		mapaMascotas = new MapaMascotas();
		listaUsuarios = new ListaUsuarios();
		mapaEspecies = new MapaEspecies();
		mapaVacunas = new MapaVacunas();
	}

   public Usuario obtenerUsuario(String rut, String pass) {
	   Usuario usuario= listaUsuarios.getUsuario(rut);
	   if(usuario != null && usuario.getContrasena().equals(pass))
		   return usuario;
	   return null;
   }
   
   public Mascota obtenerMascota(String idChip) {
	   Mascota mascota= mapaMascotas.obtenerMascota(idChip);
	   if(mascota!=null) {
		   return mascota;
	   }
	   return null;
   }
   
  

   public boolean eliminarUsuario(String rut) {
		if(listaUsuarios.eliminarUsuario(rut)) {
			return true;
		}
		return false;
	}
   
   
   
   
	
   public boolean eliminarMascota(String idChip) {
	   	 String aux = mapaMascotas.obtenerMascota(idChip).getDuenio();
		if(mapaMascotas.eliminarMascota(idChip) &&  listaUsuarios.getUsuario(aux).eliminarMascota(idChip)) {
			return true;
		}
		return false;
	}
   
   
   
   
   
   
   
	public void registraUsuario(String nombre, String rut, String edad, String direccion, String contrasena) throws NumberFormatException, SQLException, RegistroFallidoException {

			listaUsuarios.agregarUsuario(nombre, rut, Integer.parseInt(edad), direccion, contrasena);
		
	}
	
	public boolean agregarMascota(String nombre, String  idChip,String sexo, String especie,ArrayList<String> vacunas, String edad, String duenio) throws NumberFormatException, SQLException, MascotitaException {
		Especie especita = mapaEspecies.obtenerEspecie(especie);
		ArrayList<Vacuna> vacunitas = mapaVacunas.getVacunas(vacunas);
		Mascota m = null;
		if(especita == null)
			return false;
		if( (m =mapaMascotas.agregarMascota(nombre,idChip,sexo,especita,vacunitas, Integer.parseInt(edad), duenio) ) != null) {
			listaUsuarios.agregarMascota(m);
			return true;
		}
		return false;
	}

	public Duenio obtenerDuenio(String rut) {
		   Duenio usuario= (Duenio) listaUsuarios.getUsuario(rut);
		   if(usuario != null)
			   return usuario;
		   return null;
	}


}
