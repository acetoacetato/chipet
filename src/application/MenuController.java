package application;

import java.io.IOException;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class MenuController {
	
	@FXML
	private TextField txtId;
	@FXML
	public void reportePorPantalla(ActionEvent evento) {
		Stage primaryStage= new Stage();
		Parent root = null;
		
		try {
			root = FXMLLoader.load(getClass().getResource("/application/VentanaReporteMascota.fxml"));
		} catch (IOException noHayEntrada) {
			noHayEntrada.printStackTrace();
		}
		Scene scene = new Scene(root,600,400);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	@FXML
	public void eliminaUsuario(ActionEvent evento) throws SQLException {
		SistemaChip usuario = ClaseSistema.getSistemaChip();
		boolean aux=usuario.eliminarUsuario(txtId.getText());
		if(aux!=false) {
			System.out.println("eliminado");
		}
	}
	@FXML
	public void registroMascota(ActionEvent evento) {
		
		Stage primaryStage= new Stage();
		Parent root=null;
		try {
			
			root = FXMLLoader.load(getClass().getResource("/application/VentanaRegistroMascota.fxml"));
			
		} catch (IOException noHayEntrada) {
			noHayEntrada.printStackTrace();
		}
		Scene scene = new Scene(root,400,400);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
}
