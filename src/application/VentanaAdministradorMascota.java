package application;

import java.io.IOException;
import java.sql.SQLException;

import Objetos.Mascota;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class VentanaAdministradorMascota {
	
	private TextField buscarIdChip;
	
	private TextField nombreMascota;
	
	private TextField nuevoDuenio;
	
	
	public void BuscarMascota(ActionEvent evento)throws SQLException {
		SistemaChip  mascota= ClaseSistema.getSistemaChip();
		Mascota aux = mascota.obtenerMascota(buscarIdChip.getText());
		if(aux!=null) {
			nombreMascota.setText(aux.getNombre());
			nuevoDuenio.setText(aux.getDuenio());
		}
		
		
				
	}
	
	public void Aplicar(ActionEvent evento)throws SQLException {
		String chip =buscarIdChip.getText();
		SistemaChip  mascota= ClaseSistema.getSistemaChip();
		Mascota aux = mascota.obtenerMascota(chip);
		if (aux!=null) {
			aux.setNombre(nombreMascota.getText());
			aux.setDuenio(nuevoDuenio.getText());
		}
		
	}
	
	public void ListaVacunas(ActionEvent evento) {
		Stage primaryStage= new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/application/ListaVacunasMascota.fxml"));
		} catch (IOException noHayEntrada) {
			// TODO Auto-generated catch block
			noHayEntrada.printStackTrace();
		}
		Scene scene = new Scene(root,400,400);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		closeAction(evento);
		
		
	}
	
	@FXML
	private void closeAction(ActionEvent event) {
	
		 Stage stage;
	     stage=(Stage) buscarIdChip.getScene().getWindow();
	     stage.close();
	}
	
	

}
