package application;

import java.sql.SQLException;

public class ClaseSistema {

	private static SistemaChip sis;
	
	public static SistemaChip getSistemaChip() throws SQLException {
	
		if(sis == null)
			sis = new SistemaChip();
		
		return sis;
		
	}
	
}
