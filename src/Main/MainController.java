package Main;

import java.io.IOException;
import java.sql.SQLException;

import Excepciones.RegistroFallidoException;
import application.ClaseSistema;
import application.SistemaChip;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class MainController {
	

	@FXML
	private Label lblStatus;
	
	@FXML
	private TextField txtIdUsuario;
	
	@FXML
	private TextField txtContrasena;
	
	@FXML
	private TextField nombre;
	
	@FXML
	private TextField rut;

	@FXML
	private TextField edad;
	 @FXML
	 private TextField direccion;
	@FXML
	private TextField contrasena;
	@FXML
	private Button volverAtras;
	
	/**
	 * Si los parametros del id del usuario y su contrase�a son los correctos entra al men�
	 * @param evento
	 * @throws IOException
	 */
	
	public void login(ActionEvent evento) throws IOException {
		
	
		
		if(txtIdUsuario.getText().equals("1") && txtContrasena.getText().equals("2")) {
			
			lblStatus.setText("Entrando...");
			closeAction(evento);
			Stage primaryStage= new Stage();
			Parent root = null;
			try {
				root = FXMLLoader.load(getClass().getResource("/application/Main.fxml"));
			} catch (IOException noHayEntrada) {
				// TODO Auto-generated catch block
				noHayEntrada.printStackTrace();
			}
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
		}else {
			lblStatus.setText("Usuario/Contrase�a incorrecta");
		}
	}
@FXML
public void registro(ActionEvent evento) throws IOException, SQLException {
		
			SistemaChip sistema = ClaseSistema.getSistemaChip();
	
			Stage primaryStage= new Stage();
			Parent root = null;
			try {
				root = FXMLLoader.load(getClass().getResource("/application/VentanaRegistro.fxml"));
			} catch (IOException noHayEntrada) {
				// TODO Auto-generated catch block
				noHayEntrada.printStackTrace();
			}
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			closeAction(evento);
			/*if(nombre.getText()!=null && rut.getText()!=null && edad.getText()!=null && contraseña.getText()!=null && direccion.getText()!=null) {
				try {
					sistema.registraUsuario(nombre.getText(),rut.getText(),edad.getText(),direccion.getText(),contraseña.getText());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RegistroFallidoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
			
	}
	
	
	@FXML
	private void closeAction(ActionEvent event) {
	
		 Stage stage;
	     stage=(Stage) lblStatus.getScene().getWindow();
	     stage.close();
	}
	

	public void volver(ActionEvent event) {
		Stage primaryStage= new Stage();
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/application/Login.fxml"));
		} catch (IOException noHayEntrada) {
			// TODO Auto-generated catch block
			noHayEntrada.printStackTrace();
		}
		Scene scene = new Scene(root,400,400);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		Stage stage;
	    stage=(Stage) volverAtras.getScene().getWindow();
	    stage.close();
		
		
	}
	
}
