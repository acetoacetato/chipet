package Excepciones;

public class MascotitaException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8240748855954328768L;

	public MascotitaException(String msg) {
		super("Fallo de operación con mascota. " + msg);
	}
}
