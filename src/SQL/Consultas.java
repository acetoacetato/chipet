package SQL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Objetos.Especie;
import Objetos.Mascota;
import Objetos.Vacuna;


public class Consultas {
	private static Connection conect = null;
	private static Consultas consulta = null;
	private Consultas () throws ClassNotFoundException, SQLException {
		conect=Sql.getConexion();
		
	}
	
	public static Consultas getConsultas() throws SQLException {
		try {
			if(consulta == null)
				consulta = new Consultas();
		}catch(ClassNotFoundException e) {
			
		}
		
		return consulta;
	}
	
	/**
	 * Agrega una mascota a la base de datos
	 * 
	 * @param Nombre : nombre de la mascota
	 * @param IdChip : identificador del chip
	 * @param Sexo : sexo de la mascota
	 * @param Especie : especie de la mascota
	 * @param Color: color de la mascota
	 * @param Edad: edad de la mascota
	 * @throws SQLException : en caso de que no se pueda ingresar el evento a la base de datos, se lanza la excepci�n
	 */
	public void agregarMascota(String Nombre,String IdChip,String Sexo,String Especie,String Color,int Edad, boolean Vacunas) throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL agregarMascota(?,?,?,?,?,?,?)}");
		cs.setString(1,Nombre);
		cs.setString(2,IdChip);
		cs.setString(3,Sexo);
		cs.setString(4,Especie);
		cs.setString(5,Color);
		cs.setInt(6,Edad);
		cs.setBoolean(7,Vacunas);
		cs.executeUpdate();
	}
	
	
	 public ResultSet obtenerVacunasEspecie(String tipo) throws SQLException{
		 CallableStatement cs = conect.prepareCall("{CALL obtenerVacunasEspecie(?);}");
		 cs.setString(1, tipo);
		 return cs.executeQuery();
		 
	 }
	
	
	/**
	 * Obtiene a todos los usuarios (tanto due�os como administradores) en la base de datos.
	 * @return un ResultSet con el resultado de la consulta
	 * @throws SQLException en caso de que falle la conexi�n a la base de datos, se lanza la excepci�n
	 */

	public ResultSet obtenerUsuarios() throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL obtenerUsuarios()}");
		return cs.executeQuery();
	}
	/**
	 * Registra usuario en la base de datos.
	 * @param nombre
	 * @param rut
	 * @param edad
	 * @param direccion
	 * @param contrasena
	 * @return
	 */
	public void registrarUsuario(String Nombre, String Rut, int Edad, String Direccion, String Contrasena) throws SQLException {
		
			CallableStatement cs = conect.prepareCall("{CALL agregarUsuario(?,?,?,?,?)}");
			
			
			cs.setString(1, Rut);
			cs.setString(2, Nombre);
			cs.setInt(3, Edad);
			cs.setString(4, Direccion);
			cs.setString(5, Contrasena);
			
			cs.executeUpdate();

		
		
	}
	/**
	 * llama al procedimiento de obtener un usuario.
	 * @return
	 * @throws SQLException
	 */
	public ResultSet obtenerUsuario() throws SQLException{
		
		CallableStatement cs = conect.prepareCall("{CALL obtenerUsuario()}");
		
		return cs.executeQuery();
		
	}

	/**
	 * A trav�s del nombre elimina el usuario por una llamada al procedimiento eliminarUsuario.
	 * @param Nombre
	 * @throws SQLException
	 */	
	public void eliminarUsuario(String Nombre) throws SQLException {
		
		CallableStatement cs = conect.prepareCall("{CALL eliminarUsuario(?)}");
		cs.setString(1, Nombre);
		cs.executeUpdate();
		
	}

	public ResultSet obtenerVacunasMascota(String chip)throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL obtenerVacunasMascota(?)}");
		cs.setString(1, chip);
		return cs.executeQuery();
	}

	public void agregarVacunasMascota( String id, Vacuna... vacunitas) throws SQLException  {
		CallableStatement cs = conect.prepareCall("{CALL agregarVacunasMascota(?,?)}");
		cs.setString(1, id);
		for(Vacuna v : vacunitas) {
			cs.setString(2, v.getNroSerie());
			cs.executeUpdate();
		}
		
	}
	

	public void agregarVacunasEspecie( String id,ArrayList<Vacuna >vacunitas) throws SQLException  {
		CallableStatement cs = conect.prepareCall("{CALL agregarVacunasEspecie(?,?)}");
		cs.setString(1, id);
		for(Vacuna v : vacunitas) {
			cs.setString(2, v.getNroSerie());
			cs.executeUpdate();
		}
		
	}

	public void editarVacuna(String nroSerie, String nombre) throws SQLException {
		
		CallableStatement cs = conect.prepareCall("{CALL modificarVacuna(?,?)}");
		cs.setString(1, nroSerie);
		cs.setString(2, nombre);
		cs.executeUpdate();
	}

	public void eliminarVacunaMascota(String id, String nroSerie) throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL eliminarVacunaMascota(?,?)}");
		cs.setString(1, id);
		cs.setString(2, nroSerie);
		cs.executeUpdate();
	}

	public void eliminarVacunaEspecie(String id, String nroSerie) throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL eliminarVacunaEspecie(?.?)}");
		cs.setString(1,id);
		cs.setString(2, nroSerie);
	}

	public ResultSet obtenerVacunas() throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL obtenerVacunas()}");
		return cs.executeQuery();
	}

	public void agregarVacuna(Vacuna v) throws SQLException {
		
		CallableStatement cs = conect.prepareCall("{CALL agregarVacuna(?, ?)}");
		cs.setString(1, v.getNroSerie());
		cs.setString(2, v.getNombre());
		cs.executeUpdate();
	}

	public void eliminarVacuna(Vacuna v) throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL agregarVacuna(?)}");
		cs.setString(1, v.getNroSerie());
		cs.executeUpdate();
		
	}

	public ResultSet obtenerEspecies() throws SQLException {

		CallableStatement cs = conect.prepareCall("{CALL obtenerEspecies()}");
		return cs.executeQuery();
	}

	public void agregarEspecie(Especie e) throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL agregarEspecie(?,?)}");
		cs.setString(1, e.getTipo());
		cs.setString(2, e.getDescripcion());
		cs.executeUpdate();
		agregarVacunasEspecie(e.getTipo(), e.listaVacunas());
		
	}

	public void modificarEspecie(String tipo, String descripcion) throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL modificarEspecie(?,?)}");
		cs.setString(1, tipo);
		cs.setString(2, descripcion);
	}

	public void eliminarEspecie(String tipo) throws SQLException {
		CallableStatement cs = conect.prepareCall("{CALL eliminarEspecie(?)}");
		cs.setString(1, tipo);
		
	}

	
}

