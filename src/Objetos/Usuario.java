package Objetos;

import Colecciones.ListaMascotas;

public abstract class Usuario {
	private String nombre;
	private String rut;
	private int edad;
	private String contrasena;
	private String direccion;
	private ListaMascotas listaMascotas;
	
	public abstract String tipoCuenta();
	
	public String infoCuenta() {
		return "Cuenta " + nombre; 
	}
	
	
	
	public Usuario(String nombre,String rut,int edad, String contrasena, String direccion){
		setNombre(nombre);
		setRut(rut);
		setEdad(edad);
		setContrasena(contrasena);
		setDireccion(direccion);
	}
	
	public void setDireccion(String direccion) {
		this.direccion=direccion;		
	}
	
	public String getDireccion() {
		return direccion;
	}

	public String getNombre(){
		return nombre;
	}
 
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
 
	public String getRut(){
		return rut;
	}

	public void setRut(String rut){
		this.rut=rut;
	}

	public int getEdad(){
	 	return edad;
	}

	public void setEdad(int edad){
	 	this.edad=edad;
	}

	public String getContrasena() {
		return contrasena;
	}
	
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public void agregarMascota(Mascota m) {
		listaMascotas.agregarMascota(m);
		
	}
	public boolean eliminarMascota(String m) {
		return listaMascotas.eliminarMascota(m);
		
	}
	
}
