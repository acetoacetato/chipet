package Objetos;

import java.sql.SQLException;
import java.util.ArrayList;

import Colecciones.ListaVacunas;

public class Mascota {
	private String nombre;
	private String idChip;
	private String sexo;
	private int edad;
	private ListaVacunas vacunas;
	private Especie especie;
	private boolean defuncion; 
	private String duenio;
	

	public Mascota(String nombre,String idChip,String sexo,Especie especie, ArrayList<Vacuna> vacunas,int edad, boolean defuncion, String duenio) throws SQLException {
		this.nombre = nombre;
		this.idChip = idChip;
		this.sexo = sexo;
		this.especie = especie;
		this.edad = 0;
		this.duenio = duenio;
		this.vacunas = new ListaVacunas(idChip);
		this.especie = especie;
		this.defuncion = false;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre=nombre;
	}
	public String getIdChip() {
		return idChip;
	}
	public void setIdChip(String idChip) {
		this.idChip=idChip;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo=sexo;
	}
	public Especie getEspecie() {
		return especie;
	}
	public void setEspecie(Especie especie) {
		this.especie=especie;
	}

	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad=edad;
	}
	public boolean getDefuncion() {
		return defuncion;
	}
	public void setDefuncion(boolean defuncion) {
		this.defuncion=defuncion;
	}
	public String getDuenio() {
		// TODO Auto-generated method stub
		return duenio;
	}
	public void setDuenio(String text) {
		this.duenio=duenio;
	}
	
	

}