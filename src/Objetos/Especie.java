package Objetos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Colecciones.ListaVacunasEspecie;
import Colecciones.ListaVacunas;

public class Especie {
	private String tipo;
	private String descripcion;
	private ListaVacunasEspecie listaVacunas;
	
	public Especie(String tipo, String descripcion, ArrayList<Vacuna> vacunas) throws SQLException {
		this.tipo = tipo;
		this.listaVacunas = new ListaVacunasEspecie(tipo, vacunas);
	}
	
	
	public Especie(ResultSet rs) throws SQLException {
		this.tipo = rs.getString("tipo");
		this.descripcion = rs.getString("descripcion");
		listaVacunas = new ListaVacunasEspecie(tipo);
	}



	public String getTipo() {
		return tipo;
	}
	
	public void setTipo( String tipo) {
		this.tipo = tipo;
	}


	public void modificar(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public ArrayList<Vacuna> listaVacunas() {
		return listaVacunas.obtenerLista();
	}
}
