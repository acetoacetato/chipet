package Objetos;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validador {

	
	public static boolean validarRut(String rut) {
		String regex = "^([0-9]){8}-([0-9]|k|K)$";
		Pattern patron = Pattern.compile(regex);
		Matcher matcher = patron.matcher(rut);
		return matcher.find();
	}
	
	public static boolean validarSQLInjection(String str) {
		String regex = "\'|\"|--|";
		Pattern patron = Pattern.compile(regex);
		Matcher matcher = patron.matcher(str);
		return !matcher.find();
	}
	
}
