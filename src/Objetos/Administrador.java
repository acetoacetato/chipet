package Objetos;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Administrador extends Usuario{

	public Administrador(String nombre, String rut, int edad, String contrasena,String direccion) {
		super(nombre, rut, edad, contrasena,direccion);
	}
	public Administrador(ResultSet rs) throws SQLException {
		super(rs.getString("nombre"), rs.getString("rut"), rs.getInt("edad"), rs.getString("contraseña"), rs.getString("direccion"));
	}
	public String tipoCuenta() {
		return "Administrador";
	}
	
	public String infoCuenta() {
		return "cuenta tipo Administrador, nombre " + getNombre();
	}

	
}
