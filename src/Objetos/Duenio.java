package Objetos;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Duenio extends Usuario {
	public Duenio(String nombre,String rut,int edad, String direccion, String contrasena){
		super(nombre, rut, edad, direccion ,contrasena);
	}

	public Duenio(ResultSet rs) throws SQLException {
		super(rs.getString("nombre"), rs.getString("rut"), rs.getInt("edad"), rs.getString("contraseña"), rs.getString("direccion"));
	}

	public String tipoCuenta() {
		return "Duenio";
	}
	
	public String infoCuenta() {
		return "cuenta tipo Persona, nombre " + getNombre();
	}
	

}
