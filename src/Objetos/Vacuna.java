package Objetos;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Vacuna {
	private String nroSerie;
	private String nombre;
	
	public Vacuna(String nroSerie, String nombre) {
		this.nroSerie = nroSerie;
		this.nombre = nombre;
	}

	public Vacuna(ResultSet rs) throws SQLException {
		nroSerie = rs.getString("nroSerie");
		nombre = rs.getString("nombre");
	}

	public String getNroSerie() {
		return nroSerie;
	}

	public void setNroSerie(String nroSerie) {
		this.nroSerie = nroSerie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
