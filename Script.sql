create schema if not exists Chips;
use Chips;
drop table if exists Mascotas;
create table Mascotas(

	nombre varchar(90),
    chip varchar(70) primary key,
    sexo boolean,
    especie varchar(30) references Especie(tipo),
    rut varchar(10) references Usuario(rut),
	edad int,
    difunto boolean
);


drop table if exists Especie;
create table Especie(
	tipo varchar(30) primary key,
    descripcion varchar(150)
);


drop table if exists Usuario;
create table Usuario(
	rut varchar(10) primary key,
    nombre varchar(90),
    edad int,
    contraseña varchar(30),
    direccion varchar(90),
    adm boolean default false
);

drop table if exists Vacuna;
create table Vacuna(
	nroSerie varchar(50) primary key,
    nombre varchar(40) unique
);


drop table if exists VacunaMascotas;
create table VacunaMascotas(
	chip varchar(70) references Mascota(chip),
    nroSerie varchar(50) references Vacuna(nombre)
);


drop table if exists VacunaEspecie;
create table VacunaEspecie(
	tipo varchar(30) references Especie(tipo),
    nroSerie varchar(50) references Vacuna(nombre)
);








delimiter $$
drop procedure if exists agregarUsuario$$
create procedure agregarUsuario(id varchar(10), namecito varchar(50), age int, pswd varchar(50), dir varchar(70))
begin
	INSERT INTO Usuario(rut, nombre, edad, direccion, contraseña) VALUES (id, namecito, age, pswd, dir);
end
$$

delimiter $$
drop procedure if exists agregarAdmin$$
create procedure agregarAdmin(id varchar(10), namecito varchar(50), age int, pswd varchar(50), dir varchar(70))
begin
	INSERT INTO Usuario(rut, nombre, edad, direccion, contraseña, adm) VALUES (id, namecito, age, pswd, dir, true);
end
$$

delimiter $$
drop procedure if exists obtenerUsuarios$$
create procedure obtenerUsuarios()
begin
	select * 
    from Usuario;
end
$$

delimiter $$
drop procedure if exists obtenerEspecies$$
create procedure obtenerEspecies()
begin
	select * 
    from Especie;
end
$$

delimiter $$
drop procedure if exists obtenerVacunasEspecie$$
create procedure obtenerVacunasEspecie(Tipo varchar(90))
begin
	select * 
    from VacunasEspecie 
    where tipo = Tipo;
end
$$


delimiter $$
drop procedure if exists obtenerVacunas$$
create procedure obtenerVacunas()
begin
	select * 
    from Vacuna;
end
$$



CALL agregarUsuario("19903106-6", "alen", 20, "tu casa", "123momiaes");
CALL agregarAdmin("12345678-9", "alen", 20, "tu casa", "123momies");





